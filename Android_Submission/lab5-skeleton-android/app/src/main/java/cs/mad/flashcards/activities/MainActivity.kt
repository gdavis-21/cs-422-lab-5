package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import cs.mad.flashcards.runOnIO

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var daoFlashcardSet: FlashcardSetDao
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardSetDatabase::class.java, FlashcardSetDatabase.databaseName).build()
        daoFlashcardSet = db.flashcardSetDao()
        binding.flashcardSetList.adapter = FlashcardSetAdapter(daoFlashcardSet)

        binding.createSetButton.setOnClickListener {
            var newFlashcardSet: FlashcardSet = FlashcardSet("test")
            runOnIO {
                daoFlashcardSet.insert(newFlashcardSet)
            }
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(newFlashcardSet)
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }

        prefs = getSharedPreferences("MAD Flashcards", MODE_PRIVATE)

        loadData()
    }
    private fun loadData() {
        if (prefs.getBoolean("isDownloaded", false)) {
            runOnIO {
                (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(daoFlashcardSet.getAll())
            }
        } else {
            var flashcardSets: List<FlashcardSet> = FlashcardSet.getHardcodedFlashcardSets()
            flashcardSets?.let {
                (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(flashcardSets)
                runOnIO {
                    daoFlashcardSet.insertAll(flashcardSets)
                }
            }
            prefs.edit().putBoolean("isDownloaded", true).apply()
        }
    }
}