package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class Flashcard(
    var question: String,
    var answer: String,
    @PrimaryKey val id: Long? = null
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i"))
            }
            return cards
        }
    }
}

@Dao
interface FlashcardDao {
    @Query("SELECT * FROM Flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(flashcard: Flashcard)

    @Insert
    suspend fun insertAll(flashcardList: List<Flashcard>)

    @Update
    suspend fun update(flashcard: Flashcard)

    @Delete
    suspend fun delete(flashcard: Flashcard)

    @Query("DELETE FROM Flashcard")
    suspend fun deleteAll()
}