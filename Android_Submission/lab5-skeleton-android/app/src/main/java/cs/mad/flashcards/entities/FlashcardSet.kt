package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class FlashcardSet(val title: String, @PrimaryKey val id: Long? = null) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}

@Dao
interface FlashcardSetDao {
    @Query("SELECT * FROM FlashcardSet")
    suspend fun getAll(): List<FlashcardSet>
    @Insert
    suspend fun insert(flashcardSet: FlashcardSet)
    @Insert
    suspend fun insertAll(flashcardSetList: List<FlashcardSet>)
    @Update
    suspend fun update(flashcardSet: FlashcardSet)
    @Delete
    suspend fun delete(flashcardSet: FlashcardSet)
    @Query("DELETE FROM FlashcardSet")
    suspend fun deleteAll()
}