package cs.mad.flashcards.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import cs.mad.flashcards.runOnIO

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var flashcardDao: FlashcardDao
    private lateinit var flashcardSetDao: FlashcardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val dbFlashcards = Room.databaseBuilder(
            applicationContext,
            FlashcardDatabase::class.java, FlashcardDatabase.databaseName).build()

        val dbFlashcardSets = Room.databaseBuilder(
            applicationContext,
            FlashcardSetDatabase::class.java, FlashcardSetDatabase.databaseName).build()

        flashcardDao = dbFlashcards.flashcardDao()
        flashcardSetDao = dbFlashcardSets.flashcardSetDao()
        binding.flashcardList.adapter = FlashcardAdapter(flashcardDao)


        binding.addFlashcardButton.setOnClickListener {
            var newFlashcard: Flashcard = Flashcard("test", "test")
            runOnIO {
                flashcardDao.insert(newFlashcard)
            }
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(newFlashcard)
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {
            runOnIO {
                flashcardDao.deleteAll()
            }
            finish()
        }

        prefs = getSharedPreferences("MAD Flashcards", MODE_PRIVATE)

        loadData()
    }

    private fun loadData() {
        if (prefs.getBoolean("isDownloadedFlashcards", false)) {
            runOnIO {
                (binding.flashcardList.adapter as FlashcardAdapter).update(flashcardDao.getAll())
            }
        } else {
            var flashcards: List<Flashcard> = Flashcard.getHardcodedFlashcards()
            flashcards?.let {
                (binding.flashcardList.adapter as FlashcardAdapter).update(flashcards)
                runOnIO {
                    flashcardDao.insertAll(flashcards)
                }
            }
            prefs.edit().putBoolean("isDownloadedFlashcards", true).apply()
        }
    }
}