//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSetEntity] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
        
        loadData()
    }

    
    @IBAction func addNewSet(_ sender: Any) {
        let context = (UIApplication.shared.delegate as!
            AppDelegate).persistentContainer.viewContext
        let newFlashcardSet = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSetEntity", into: context) as! FlashcardSetEntity
        newFlashcardSet.title = "Title \(sets.count + 1)"
        sets.append(newFlashcardSet)
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup method just makes it look nice
        cell.setup()
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    //another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    func loadData() {
        let context = (UIApplication.shared.delegate as!
            AppDelegate).persistentContainer.viewContext
        if (UserDefaults.standard.bool(forKey: "isFlashcardSetsDownloaded")) {
            print("Data already downloaded!")
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardSetEntity")
            do {
                let flashcardSets = try context.fetch(fetchRequest) as! [FlashcardSetEntity]
                updateData(flashcardSets: flashcardSets)
            }
            catch {
                print(error)
            }
        }
        else {
            var flashcardSets: [FlashcardSetEntity] = []
            UserDefaults.standard.set(true, forKey: "isFlashcardSetsDownloaded")
            for flashcardSet in FlashcardSet.getHardCodedCollection() {
                let newFlashcardSet = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSetEntity", into: context) as! FlashcardSetEntity
                newFlashcardSet.title = flashcardSet.title
                flashcardSets.append(newFlashcardSet)
            }
            do {
                try context.save()
            }
            catch {
                print(error)
            }
            self.updateData(flashcardSets: flashcardSets)
        }
    }
    
    func updateData(flashcardSets: [FlashcardSetEntity]) {
        self.sets = flashcardSets
        self.collectionView.reloadData()
    }
    
    
}

