//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit
import CoreData

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate
{
    var cards: [FlashcardEntity] = []
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        tableView.addGestureRecognizer(longPressedGesture)
        makeItPretty()
        loadData()
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let context = (UIApplication.shared.delegate as!
            AppDelegate).persistentContainer.viewContext
        let newFlashcard = NSEntityDescription.insertNewObject(forEntityName: "FlashcardEntity", into: context) as! FlashcardEntity
        newFlashcard.term = "Term \(cards.count + 1)"
        newFlashcard.definition = "Definition \(cards.count + 1)"
        cards.append(newFlashcard)
        tableView.reloadData()
    }
    
    //delete cards
    @IBAction func deleteCards(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            for flashcard in cards {
                context.delete(flashcard)
            }
            self.cards.removeAll()
            try context.save()
            tableView.reloadData()
        } catch {}
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
                return
            }
        let p = gestureRecognizer.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        selectedIndex = indexPath?.row ?? 0
        if let indexPath = tableView.indexPathForRow(at: p) {
            createCustomAlert(card: cards[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "\(cards[indexPath.row].term)", message: "\(cards[indexPath.row].definition)", preferredStyle: .alert)
        selectedIndex = indexPath.row
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            self.createCustomAlert(card: self.cards[indexPath.row])
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(card: FlashcardEntity)
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "EditAlertViewController") as! EditAlertViewController
        alertVC.parentVC = self
        alertVC.card = card
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: false, completion: nil)
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    
    func loadData() {
        let context = (UIApplication.shared.delegate as!
            AppDelegate).persistentContainer.viewContext
        if (UserDefaults.standard.bool(forKey: "isFlashcardsDownloaded")) {
            print("Data already downloaded!")
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardEntity")
            do {
                let flashcards = try context.fetch(fetchRequest) as! [FlashcardEntity]
                updateData(flashcards: flashcards)
            }
            catch {
                print(error)
            }
        }
        else {
            var flashcards: [FlashcardEntity] = []
            UserDefaults.standard.set(true, forKey: "isFlashcardsDownloaded")
            for flashcard in Flashcard.getHardCodedCollection() {
                let newFlashcard = NSEntityDescription.insertNewObject(forEntityName: "FlashcardEntity", into: context) as! FlashcardEntity
                newFlashcard.definition = flashcard.definition
                newFlashcard.term = flashcard.term
                newFlashcard.missed = flashcard.missed
                flashcards.append(newFlashcard)
            }
            do {
                try context.save()
            }
            catch {
                print(error)
            }
            self.updateData(flashcards: flashcards)
        }
    }
    
    func updateData(flashcards: [FlashcardEntity]) {
        self.cards = flashcards
        self.tableView.reloadData()
    }
}
